<?php

declare(strict_types=1);

namespace GreenHouse\DoctrineOdmTypes;

use Doctrine\DBAL\Types\ConversionException;

abstract class StringableObjectArrayType extends NormalizableType
{
    final protected function normalize($value): array
    {
        return array_map(function (object $item): string { return parent::normalize($item); }, $value);
    }

    /**
     * @throws ConversionException
     */
    final protected function checkDatabaseValue($value): void
    {
        if (!empty($value) && !is_array($value)) {
            throw new ConversionException('Must be an array');
        }
    }

    /**
     * @throws ConversionException
     */
    final protected function checkPhpValue($value): void
    {
        if (!is_array($value)) {
            throw new ConversionException('Must be an array');
        }
    }

    final protected function getDenormalizationExpression(): string
    {
        return parent::getDenormalizationExpression() . '[]';
    }
}