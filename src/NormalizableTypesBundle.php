<?php

declare(strict_types=1);

namespace GreenHouse\DoctrineOdmTypes;

use GreenHouse\SymfonyExtensions\Bundle;
use Doctrine\DBAL\Types\Type;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;

final class NormalizableTypesBundle extends Bundle
{
    protected function createContainerExtension(): ?ExtensionInterface
    {
        return null;
    }

    public function boot()
    {
        foreach (Type::getTypesMap() as $name => $class) {
            if (is_a($class, NormalizableType::class, true)) {
                /** @var ObjectArrayType $type */
                $type = Type::getType($name);
                $type->setDenormalizer($this->container->get('serializer'));
                $type->setNormalizer($this->container->get('serializer'));
            }
        }
    }
}