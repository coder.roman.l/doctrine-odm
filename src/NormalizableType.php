<?php

declare(strict_types=1);

namespace GreenHouse\DoctrineOdmTypes;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\JsonType;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

abstract class NormalizableType extends JsonType implements NormalizerAwareInterface, DenormalizerAwareInterface
{
    use NormalizerAwareTrait, DenormalizerAwareTrait;

    abstract protected function getObjectClass(): string;

    /**
     * @throws ConversionException
     */
    abstract protected function checkDatabaseValue($value): void;

    /**
     * @throws ConversionException
     */
    abstract protected function checkPhpValue($value): void;

    /**
     * @throws ConversionException
     */
    final public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        $value = parent::convertToPHPValue($value, $platform);

        if (null === $value) {
            return null;
        }
        if (!is_array($value)) {
            throw new ConversionException('Must be an array');
        }

        $this->checkDatabaseValue($value);

        return $this->denormalize($value);
    }

    /**
     * @throws ConversionException
     */
    final public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (null === $value) {
            return null;
        }

        $this->checkPhpValue($value);

        return parent::convertToDatabaseValue($this->normalize($value), $platform);
    }

    protected function normalize($value) /* : mixed */
    {
        if (!$this->getNormalizer()->supportsNormalization($value)) {
            throw new ConversionException('Normalization not supported');
        }

        return $this->getNormalizer()->normalize($value);
    }

    /**
     * @throws ConversionException
     */
    private function denormalize(array $value) /* : mixed */
    {
        if (!$this->getDenormalizer()->supportsDenormalization($value, $this->getObjectClass())) {
            throw new ConversionException('Denormalization not supported');
        }

        return $this->getDenormalizer()->denormalize($value, $this->getDenormalizationExpression(), JsonEncoder::FORMAT);
    }

    protected function getDenormalizationExpression(): string
    {
        return $this->getObjectClass();
    }

    private function getNormalizer(): NormalizerInterface
    {
        return $this->normalizer;
    }

    private function getDenormalizer(): DenormalizerInterface
    {
        return $this->denormalizer;
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return parent::getSQLDeclaration(array_merge($fieldDeclaration, ['jsonb' => true]), $platform);
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform)
    {
        return true;
    }
}